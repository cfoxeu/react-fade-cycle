import React, { Component } from 'react';

let style = {};

const dummyTimer = (fn, hour) => {
    setTimeout(fn, hour);
};

export default class Fader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            background: '#ccc',
            newBg: '#555'
        }
    }
    ping() {
        console.log('Ping!');
        this.setState({ background: this.props.dayBg });
        setTimeout(this.setState({ background: this.props.nightBg }), 800);
    };
    componentDidMount() {
        console.log("Day: ", this.props.dayBg);
        console.log("Night: ", this.props.nightBg);
        dummyTimer(this.ping, 1000);
    }
    render() {
        const style = this.state.background;
        return (
            <div className="fader" style={{ background: style }}></div>
        );
    }
}
