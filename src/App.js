import React, { Component } from 'react'
import './index.less'

import Fader from './Fader'

const interval = 100;

export default class App extends Component {
   render() {
      return (
        <div className="app">
            <Fader dayBg="#269" nightBg="#125" time={{interval}} />
        </div>
      );
   }
}
